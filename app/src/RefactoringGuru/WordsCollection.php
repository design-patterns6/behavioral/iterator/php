<?php

declare(strict_types=1);

namespace Iterator\RefactoringGuru;

class WordsCollection implements \IteratorAggregate
{
    private array $items = [];

    public function getItems(): array
    {
        return $this->items;
    }

    public function addItem(string $item): void
    {
        $this->items[] = $item;
    }

    public function getIterator(): AlphabeticalOrderIterator
    {
        return new AlphabeticalOrderIterator(collection: $this);
    }

    public function getReverseIterator(): AlphabeticalOrderIterator
    {
        return new AlphabeticalOrderIterator(collection: $this, reverse: true);
    }
}
