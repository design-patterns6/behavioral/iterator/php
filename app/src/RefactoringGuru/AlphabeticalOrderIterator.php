<?php

declare(strict_types=1);

namespace Iterator\RefactoringGuru;

class AlphabeticalOrderIterator implements \Iterator
{
    public function __construct(
        private readonly WordsCollection $collection,
        private readonly bool $reverse = false,
        private int $position = 0,
    ) {
    }

    public function rewind(): void
    {
        $this->position = $this->reverse
            ? count($this->collection->getItems()) - 1
            : 0;
    }

    public function current(): string
    {
        return $this->collection->getItems()[$this->position];
    }

    public function key(): int
    {
        return $this->position;
    }

    public function next(): void
    {
        $this->position += $this->reverse ? -1 : 1;
    }

    public function valid(): bool
    {
        return isset($this->collection->getItems()[$this->position]);
    }
}
