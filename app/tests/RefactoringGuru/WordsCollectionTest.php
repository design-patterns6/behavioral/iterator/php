<?php

declare(strict_types=1);

namespace Iterator\Tests\RefactoringGuru;

use Iterator\RefactoringGuru\WordsCollection;
use PHPUnit\Framework\TestCase;

class WordsCollectionTest extends TestCase
{
    public function testStraightTraversal(): void
    {
        // Arrange
        $collection = new WordsCollection();
        $collection->addItem("First");
        $collection->addItem("Second");
        $collection->addItem("Third");

        // Act
        $result = [];

        foreach ($collection->getIterator() as $item) {
            $result[] = $item;
        }

        // Assert
        $this->assertSame(['First', 'Second', 'Third'], $result);
    }

    public function testReverseTraversal(): void
    {
        // Arrange
        $collection = new WordsCollection();
        $collection->addItem("First");
        $collection->addItem("Second");
        $collection->addItem("Third");

        // Act
        $result = [];

        foreach ($collection->getReverseIterator() as $item) {
            $result[] = $item;
        }

        // Assert
        $this->assertSame(['Third', 'Second', 'First'], $result);
    }
}
